/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import appIndex from './app/src/app';

AppRegistry.registerComponent(appName, () => appIndex);
